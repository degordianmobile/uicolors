

function layer(context, layer) {

    var jsonObjects = []

    processFillLayerObject(layer, jsonObjects)
    processShadowLayerObject(layer, jsonObjects)
    processTextLayerObject(layer, jsonObjects)

    var JSONString = JSON.stringify(jsonObjects, null, 2)

    if (jsonObjects.length == 0) {
        return null
    }

    return {
        code: JSONString,
        language: "json"
    }
}

function getStringForColor(color) {
    return `UIColor(red: ${color.r}/255, green: ${color.g}/255, blue: ${color.b}/255, alpha: ${color.a})`
}

function processTextLayerObject(layer, jsonObjects) {
    layer.textStyles.forEach(style => {
        var textObjects = []

        if (style.textStyle) {
            var textObject = {
                "color": getStringForColor(style.textStyle.color)
            }
            textObjects.push(textObject)
        }

        var jsonObject = {
            "text": textObjects
        }
        jsonObjects.push(jsonObject)
    })
}

function processShadowLayerObject(layer, jsonObjects) {
    layer.shadows.forEach(shadow => {
        var shadowObjects = []

        if (shadow.color) {
            var shadowObject = {
                "color": getStringForColor(shadow.color)
            }
            shadowObjects.push(shadowObject)
        }

        var jsonObject = {
            "shadow": shadowObjects
        }
        jsonObjects.push(jsonObject)
    })
}

function processFillLayerObject(layer, jsonObjects) {
    layer.fills.forEach(fill => {

        var fillObjects = []

        if (fill.color) {
            var fillObject = {
                "color": getStringForColor(fill.color)
            }
            fillObjects.push(fillObject)
        }

        if (fill.gradient) {
            var colorObjects = []

            fill.gradient.colorStops.forEach(colorStop => {
                colorObjects.push(getStringForColor(colorStop.color))
            })

            var fillObject = {
                "gradient-colors": colorObjects
            }
            fillObjects.push(fillObject)
        }

        var jsonObject = {
            "fill": fillObjects
        }
        jsonObjects.push(jsonObject)
    })
}